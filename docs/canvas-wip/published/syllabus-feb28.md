#### [**Computer and Information Sciences (CISC) 179 - Intro to Python Programming**]{style="color: #236fa1;"}

**Instructor:** Hobson Lane

**Instructor email:** <lane@totalgood.com>

**Course of Record Number (CRN)**: 53133

### Description

This is an introductory course in Python programming, incorporating the
fundamentals of object oriented software and user interface design. You
will learn how to program a computer such that it can read from input
devices (keyboard and files), write to output devices (console and
files).

You will learn how to:

-   Analyze user needs and requirements
-   Design a user interface for a Python application
-   Create classes with attributes and methods for implementing your
    program
-   Write software for event procedures and business logic
-   Test and debug the completed programs and applications
-   Document your program for users and developers using Python
    "doctests"

This course is intended for Computer and Information Sciences majors or
anyone interested in the Python programming language.

### Important dates

-   01/29/2024 - Start of Spring Semester
-   [02/09/2024 - Drop deadline]{style="color: #34495e;"} (drop class
    with full refund and no "W" on transcript)
-   02/16/2024 - 02/19/2024 - Lincoln/Washington Day (CAMPUS CLOSED)
-   03/25/2024 - 03/29/2024 - (SPRING BREAK)
-   03/29/2024 - Cesar Chavez Day (CAMPUS CLOSED)
-   04/12/2024 - Withdrawal Deadline (Primary 16-Wk Session)
-   04/30/2024 - Deadline to Apply for Graduation
-   05/25/2024 - End of Spring Semester

### Prerequisites

    •  High School Algebra

 

### Learning objectives and schedule

**Week 1.** Python programming language,  variables and types

        Sign into Runestone.Academy, read a chapter, and run code
examples in the browser.

        FOPP-1: Edit and run Python expressions and programs from the
command line

**Week 2.** Debugging and object oriented programming

        FOPP-2: Set and examine variable values and understand basic
types (`str`, `float`, and `int` )

        FOPP-3: Error types, error messages, finding and correcting
logic and syntax errors.

Week 3.  Sequences and iteration

        FOPP-4: Import and use modules, namespaces, and object methods.

        FOPP-5: : Use the Python `turtle` module to visualize program
control flow in for loops.   

**Week 4.**

        Project: Install Python and a text editor (Spyder, PyCharm or
SublimeText) or IDE

        FOPP-6: Indexing, slicing, and combining lists and strings
(`list`, `str`, `\[start:stop\]`)

        FOPP-7: Iterating through sequences with `for` and `while`
loops

**Week 5.**  Conditional expressions and modifying sequences

        Project: Create a single-turn text adventure game using
`input()`, `print()`, `if`, `else`.

        Project: Run a Python program (\*.py) file from command line.

        FOPP-8: Use conditional expressions (`if`, `else`) to
control program flow

        FOPP-9: Modifying sequences and the accumulator pattern

**Week 6.**  Files and dictionaries

        Project: Create a text adventure game with 5+ turns using hard
coding.

        FOPP-10: Reading and writing files (`open`, `read`, and
`write`)

        FOPP-11: Dictionaries (nested `dict` containers)

**Week 7.**  Functions and tuples

        FOPP-12: Functions  (`def`)

        FOPP-13: Tuples (`tuple()`)

        MIDTERM EXAM

**Week 8.**  Midterm project - Build a text adventure game

        Define container variables (`list`, `tuple`, `dict`) with
game content (`str`, `int`) \
        Use `input` and `output` to interact with the user through
the console (terminal)\
        Use conditionals and looping to handle invalid user input
(`while` and/or `for)

**Week 9.**  Advanced iteration patterns and functions

        FOPP-14: Advanced iteration

        FOPP-15: Advanced functions and `kwargs`

**Week 10.  **

        FOPP-16: Sorting sequences

        FOPP-17: Nested data

**Week 11.  **

        FOPP-18: Test cases

        FOPP-19: Exceptions (`try` and `except`)

**Week 12.  **

        FOPP-20:  Classes and objects

        FOPP-21: Building Programs

**Week 13. FINAL EXAM**

        Designing, building, testing and debugging Python programs from
scratch.

        Understanding nested data structures and flat file formats for
data-driven Python programs

Week 14. Installing and using external Python packages (`pandas` and
`doctest`)

        Pandas Python package for reading and writing HTML and CSV
tables.

        Project: Move content from hard-coded Python variable
definitions to a Spreadsheet

Week 15.

        Final Project: Load text-adventure game (or chatbot) content
from CSV.

        Final Project: Use functions, loops, and text files (CSV) to
create data-driven Python programs

### Grading

Your final grade will be the weighted average of your accumulated scores
on reading assignments, quizzes, programming assignments (labs) and
exams.

15%: 15 Weekly reading assignments in Runestone (1 point per
assignment)\
15%: 15 Weekly quizzes in Runestone (1 point per quiz)\
15%: Midterm exam in Runestone\
15%: Midterm project (programming assignment)\
      5%: Does the program run without syntax errors\
      5%: Does the program contain all the required elements of the
assignment\
      3%: Does the program utilize correct Python style (PEP8)\
      2%: Does the program utilize comments appropriately\
20%: Final exam in Runestone\
20%: Final project (programming assignment)\
      7%: Does the program run without syntax errors\
      5%: Does the program contain all the required elements of the
assignment\
      5%: Does the program utilize correct Python style (PEP8)\
      3%: Does the program utilize doctests and docstrings
appropriately\
\
