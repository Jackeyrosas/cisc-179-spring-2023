# Runestone Chapter 06 Questions

This one has better explanation and test data and corrects a bug in the original?

```rst
.. activecode:: mesa06_neg_index
   :language: python
   :autograde: unittest
   :practice: T

   Retrieve the last character of the ``text`` string variable and assign it to a new variable called ``last_char``. Do this so that your code will work no matter how long or short the string in ``text`` is.
   ~~~~
   text = "There's a special integer you can use to retrieve the last character of any string, no matter how long or short it is. And you never have to count characters or reading the text at all!"

   =====

   from unittest.gui import TestCaseGui

   class myTests(TestCaseGui):

      def testThree(self):
         self.assertEqual(last_char, "!", "The last character here is the same!")

   myTests().main()
```

This one has better question natural language explanation than the question it was based on.

```rst
.. activecode:: mesa06_0_offset
   :language: python
   :autograde: unittest
   :practice: T

   Retrieve the value of the eighth (8th) element within the tuple variable ``x`` using an integer index within square brackets (``[]``). Assign this value from the tuple to the variable ``object8``. Don't forget that Python starts counting index positions with zero (``0``)!
   ~~~~
   x = ("one"[0], "2", ['три', 'tres', 'trois'], 'quatro', range(5), 6.0, 7, 2**4 / 2.0, '1234567890'[-2], 1e1)

   =====

   from unittest.gui import TestCaseGui

   class myTests(TestCaseGui):

      def testOne(self):
         self.assertEqual(object8, x[7], "Testing that the variable `object8` contains the correct object.")

   myTests().main()
```

This one has slightly better variable names and Test class names:

```rst
.. activecode:: mesa_sublist
   :language: python
   :autograde: unittest
   :practice: T

   Create a new list using the 9th through 12th elements (4 items in all) of ``lst`` and assign it to the variable ``lst_slice``.
   ~~~~
   lst = ["1", "two", "tres", "quatro", 'cinco', 6.66666, "lucky 7", 8e8, 9, ["10"]*10, "11", 12] + list(range(13, 42))

   =====

   from unittest.gui import TestCaseGui

   class SliceTests(TestCaseGui):

      def test_elements(self):
         self.assertEqual(lst_slice, lst[8:12], "Testing that ``lst_slice`` has the correct elements assigned.")

      def test_length(self):
         self.assertEqual(len(lst_slice), 4, "Testing that ``lst_slice`` has 4 elements.")

   SliceTests().main()
```