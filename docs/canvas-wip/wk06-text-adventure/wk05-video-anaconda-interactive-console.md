# Week 5: Install Anaconda to and run your first Python program

This week you will learn how to install and run Anaconda so you can test your understanding of Python using an interactive console.
An interactive console, such as `iPython` or `qtPython` gives you immediate feedback on each line of code.
If you've ever used a debugger for other compiled programming languages, such as _Rust_ or _C++_, you are already familiar with interactive consoles.
Python is an interpretted language, so you can use the interactive console a lot more freely if you want to accelerate your workflow.

You will see how to implement the accumulator pattern using the following Python concepts:
- conditional expressions (`if`)
- loops (`for` and `in`)
- variable assignment (`=`)
- zero-offset indexing in Python (`[` and `range()`)
- importing the `random` module
- display text (`print`)