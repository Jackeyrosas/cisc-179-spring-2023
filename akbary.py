
def is_palindrome(s):
    s = s.replace(" ", "").lower()
    return s == s[::-1]


print("Hi, welcome to the land of horror!")
begin = input("Would you like to begin? ")

if begin.lower() == "yes":
    name = input('Please enter your name: ')
    print("Let's begin, {}!".format(name))
else:
    print("I guess you are too scared, come back when you have matured!")

# number oneeeee

room1 = ("There are two options,the door to your right which contains a skinny pathway where you may fall do your death and burn in the lava, and the door to your left, which contains hungry sleeping wolves.")
answer1 = ['left', 'l']
question1 = "Choose: left or right: "

room2 = "I will have you solve a math problem."
question2 = "4x4+4x4+4-4x4 = "
answer2 = "20"

room3 = "I am a fan of palindromes."
question3 = "I would love to hear what you come up with. "


rooms = [
    [room1, question1, answer1],
    [room2, question2, answer2],
    [room3, question3, ''],
]


roomnum = 0
for room in rooms:
    print(room[0])
    user_answer = input(room[1])
    if roomnum == 2:
        if is_palindrome(user_answer):
            roomnum += 1
        else:
            print("Wrong answer! You have failed to proceed.")
            break
    else:
        if user_answer.lower() in room[2]:
            roomnum += 1
        else:
            print("Wrong answer! You have failed to proceed.")
            break

if roomnum == 3:
    print("Congratulations! You are free!")
else:
    print("You will be my slave!")
