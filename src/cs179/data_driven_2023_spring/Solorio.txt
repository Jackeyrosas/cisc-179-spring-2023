Welcome to the text adventure game!

You wake up in a dark room. You can't see anything.
What do you want to do?
Look around.
Try to find a light switch.
Scream for help.

Enter your choice (1, 2, or 3):
You reach around in the darkness and eventually find a door.
You find a light switch and turn on the lights.
Nobody hears you. You are stuck here forever.

You are in a room with a table and a chair.
There is a piece of paper on the table.
What do you want to do?
1. Read the paper.
2. Look under the table.
3. Leave the room.
Enter your choice 1, 2, or 3

The paper reads: 'The code is 2012'.
You have already read the paper.
You find a key under the table.

You are in a hallway.
There are two doors, one on the left and one on the right.")
What do you want to do?")
1. Try the door on the left.
2. Try the door on the right.
3. Go back to the previous room.

Enter your choice 1, 2, or 3
You search the bookshelves and find a hidden passage behind them.
You pick up a book and start reading, losing track of time.
When you look up, the room has transformed into a different place.

You crawl through the hidden passage and find yourself in a secret chamber.
There is a mysterious artifact on a pedestal in the center of the room.
What do you want to do?
1. Examine the artifact.
2. Take the artifact.
3. Leave the room.

Enter your choice 1, 2, or 3
As you examine the artifact, you feel a strange energy emanating from it.
Suddenly, the room begins to shake, and the artifact crumbles into dust.
You barely make it out of the room before it collapses completely.

You take the artifact, but as soon as you do, the room starts to crumble.
You make a mad dash for the exit, narrowly escaping the collapsing chamber.

Congratulations, you have won the game!