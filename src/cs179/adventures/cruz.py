
# Intro starts here
play = input("Welcome to the Dream Escape! Would you like to play? (Yes/No)")

if play == "Yes" or play == "yes":
    print("Great! Let's begin the fun!")
    print("You wake up in a pitch dark room, illuminated by a door, you ponder if you should open it.")
elif play == "No" or play == "no":
    print("No worries! Maybe next time!")
else:
    print("Please choose Yes or No to begin.")

print("Options:")
print("1. Explore door")
choice = input("Enter your choice:")

print("You enter the hotel and are presented with three rooms to check into.")
print("Which room would you like to check into?")
print("1. House on clouds")
print("2. Pizza party")
print("3. Sewer underneath")
print("4. Music Room")
print("5. Infinite Cornfield")
choice_1 = input("Enter your choice:")

if choice_1 == "1":
    print("House on clouds")
    print("Woah! Where am I? I can see the entire city from here.")
    print("Options:")
    print("1. Open the door in front of you")
    print("2. Jump off of the cloud")
    choice_2 = input("Enter your choice:")
    if choice_2 == "1":
        print("You open the door and realize it leads you into outer space.")
    elif choice_2 == "2":
        print("You jump off the cloud... and wake up in your bed. It was all just a dream.")
    else:
        print("Invalid choice.")

elif choice_1 == "2":
    print("Pizza party")
    print("Are those clowns? Why are they holding balloons?")
    print("Options:")
    print("1. Explore the room")
    print("2. Fight your fear and take the clowns on")
    choice = input("Enter your choice:")
    if choice == "1":
        print("You explore the room and find an exit.")
    elif choice == "2":
        print("You ball your fists up and punch the clown as hard as you can. You wake up in a cold sweat in relief knowing it was all a dream.")

elif choice_1 == "3":
    print("Sewer underneath")
    print("It smells a bit funky here. I wonder where that slide would take me.")
    print("Options:")
    print("1. Walk around")
    print("2. Take the slide")

    choice = input("Enter your choice:")
    if choice == "1":
        print("You find a flashlight. Suddenly, you hear a noise in the distance. Seek cover before it finds you.")
    elif choice == "2":
        print("You find yourself in the void of eternity. You wake up. You were in a coma.")
    else:
        print("Invalid choice.")

elif choice_1 == "4":
    print("Music Room")
    print("Where is that winding sound coming from?")
    print("Options:")
    print("1. Be patient and try to figure out where the sound is coming from")
    print("2. Run around until you find the winding sound")
    choice = input("Enter your choice:")
    if choice == "1":
        print("A few seconds pass by and the sound stops.")
    elif choice == "2":
        print("As you're running around, you begin to hear footsteps chasing you from behind.")

elif choice_1 == "5":
    print("Infinite Cornfield")
    print("It's close to sunset and my phone has no signal. I'm trapped in the middle of a cornfield. Oh hey! A cell tower. Maybe I can get a signal if I get closer to it... It may take me some time though. It's pretty far away from me...")
    print("Options:")
    print("1. Run towards the cellphone tower in hopes of finding cell-service")
    print("2. Wait until someone comes and finds you")
    choice = input("Enter your choice:")
    if choice == "1":
        print("It's dark now but I made it. My phone isn't getting any service. I'm hungry and scared. Are those chainsaws I hear?")
    elif choice == "2":
        print("No one saved you. You die a few days later because of starvation")

# Still want to add more to the story and make it a bit more descriptive.
