import constants as c
import enemy as en
import item as it
import player as p
import room as r


class Game:
    def __init__(self):
        #   Parse for room descriptions
        _room_descriptions = {}
        with open("descriptions.txt") as f:
            for line in f:
                (key, val) = line.strip().split("; ")
                _room_descriptions[key] = val

        #   Create the game's final boss
        self.empress = en.Enemy(kind=c.EMPRESS, health=10000, damage=50)

        #   Create items
        _sword = it.Item('weapon', c.SWORD, 5)
        _light_arrow = it.Item('weapon', c.LIGHT_ARROW, 100, charges=5)
        _shield = it.Item('defense', c.SHIELD, defense=10)
        _cloak = it.Item('gear', c.CLOAK)
        _shovel = it.Item('gear', c.SHOVEL)
        _bomb = it.Item('gear', c.BOMB)
        _power_up_one = it.Item('power up', c.POWER_UP_ONE)
        _power_up_two = it.Item('power up', c.POWER_UP_TWO)
        _key_one = it.Item('key', c.KEY_ONE)
        _key_two = it.Item('key', c.KEY_TWO)

        #   Build rooms
        _start_room = r.Room(['N', 'S', 'E', 'W'], [], (c.STARTING_ROOM, _room_descriptions[c.STARTING_ROOM]))
        _west_corridor_north = r.Room(['N', 'S', 'E'], [], (c.WEST_CORRIDOR_NORTH, _room_descriptions[c.WEST_CORRIDOR_NORTH]))
        _west_corridor_south = r.Room(['N', 'S', 'E'], [_shovel], (c.WEST_CORRIDOR_SOUTH, _room_descriptions[c.WEST_CORRIDOR_SOUTH]))
        _west_cell = r.Room(['S'], [_sword], (c.WEST_CELL, _room_descriptions[c.WEST_CELL]))
        _east_cell = r.Room(['S'], [_shield], (c.EAST_CELL, _room_descriptions[c.EAST_CELL]))
        _staircase = r.Room(['W', 'E'], [], (c.STAIRCASE, _room_descriptions[c.STAIRCASE]))
        _barracks = r.Room(['N', 'S', 'W'], [], (c.BARRACKS, _room_descriptions[c.BARRACKS]))
        _proving_ground = r.Room(['N', 'S', 'E'], [_key_one], (c.PROVING_GROUND, _room_descriptions[c.PROVING_GROUND]))
        _graveyard = r.Room(['N', 'W'], [_key_two, _power_up_one, _light_arrow], (c.GRAVEYARD, _room_descriptions[c.GRAVEYARD]))
        _chapel = r.Room(['N', 'S'], [], (c.CHAPEL, _room_descriptions[c.CHAPEL]))
        _east_corridor = r.Room(['N', 'W'], [], (c.EAST_CORRIDOR, _room_descriptions[c.EAST_CORRIDOR]))
        _northeast_corridor = r.Room(['S', 'W'], [_bomb, _power_up_two], (c.NORTHEAST_CORRIDOR, _room_descriptions[c.NORTHEAST_CORRIDOR]))
        _throne_room = r.Room(['N', 'S', 'E', 'W'], [], (c.THRONE_ROOM, _room_descriptions[c.THRONE_ROOM]))
        _spire = r.Room(['E'], [_cloak], (c.SPIRE, _room_descriptions[c.SPIRE]))
        _empress_keep = r.Room([], [], (c.EMPRESS_KEEP, _room_descriptions[c.EMPRESS_KEEP]))

        #   Connect rooms
        _start_room.connect_rooms({('N', _throne_room), ('S', _barracks), ('E', _east_corridor), ('W', _west_corridor_north)})
        _west_corridor_north.connect_rooms({('N', _west_cell), ('S', _west_corridor_south), ('E', _start_room)})
        _west_corridor_south.connect_rooms({('N', _west_corridor_north), ('S', _staircase), ('E', _barracks)})
        _west_cell.connect_rooms({('S', _west_corridor_north)})
        _east_cell.connect_rooms({('S', _east_corridor)})
        _staircase.connect_rooms({('W', _west_corridor_south), ('E', _proving_ground)})
        _barracks.connect_rooms({('N', _start_room), ('S', _proving_ground), ('W', _west_corridor_south)})
        _proving_ground.connect_rooms({('N', _barracks), ('S', _staircase), ('E', _graveyard)})
        _graveyard.connect_rooms({('N', _chapel), ('W', _proving_ground)})
        _chapel.connect_rooms({('N', _northeast_corridor), ('S', _graveyard)})
        _east_corridor.connect_rooms({('N', _east_cell), ('W', _start_room)})
        _northeast_corridor.connect_rooms({('S', _chapel), ('W', _throne_room)})
        _throne_room.connect_rooms({('N', _empress_keep), ('S', _start_room), ('E', _northeast_corridor), ('W', _spire)})
        _spire.connect_rooms({('E', _throne_room)})

        #   Store all the rooms in a
        self.rooms = [_start_room, _west_corridor_north, _west_corridor_south, _west_cell, _east_cell, _staircase,
                      _barracks, _proving_ground, _graveyard, _chapel, _east_corridor, _northeast_corridor,
                      _throne_room, _spire, _empress_keep]
        self.player = p.Player(_sword, _light_arrow, _shield, _cloak, _shovel, _bomb, _power_up_one, _power_up_two, _key_one, _key_two)
        self.game_over = False

        print(f"{c.GREEN}Welcome to {c.RED}The Empress's Perseverance{c.GREEN}. Good luck.{c.RESET}\n")

    #   Display room information and pick up any items currently available.
    def enter_room(self, room):
        print(f"{c.VIOLET}========================================================={c.RESET}\n"
              f"Now entering:")
        room.print_room()
        if room.description[0] == c.EMPRESS_KEEP:
            self.empress_battle()

        #   Loop through any items in the room and see if the player can pick them up
        for item in room.items:
            #   Only check the items that aren't already active
            if not item.active:
                #   Criteria for each item of whether they can be picked up.
                allow_pickup =\
                    (
                        item.name == c.SWORD
                        or ((item.name == c.POWER_UP_ONE or item.name == c.KEY_TWO) and self.player.key_one.active)
                        or (item.name == c.KEY_ONE and self.player.sword.active)
                        or (item.name == c.LIGHT_ARROW and self.player.shovel.active)
                        or (item.name == c.CLOAK and self.player.power_up_one.active and self.player.sword.active)
                        or (item.name == c.SHIELD and self.player.sword.active)
                        or (item.name == c.SHOVEL and self.player.sword.active)
                        or (item.name == c.BOMB and self.player.cloak.active)
                        or (item.name == c.POWER_UP_TWO and self.player.power_up_one.active and self.player.light_arrow.active)
                    )
                if allow_pickup:
                    item.active = True
                    print(f"Picking up item {c.YELLOW}{item.name}{c.RESET}")
                    self.player.pick_up_item(item.name)

    def take_user_input(self, room):
        prompt = (f"\n{c.VIOLET}These are your options:{c.RESET}\n"
                  f"Enter a direction: {c.BLUE}{room.dirs}{c.RESET}\n"
                  f"Check stats: {c.BLUE}C{c.RESET}\n"
                  f"Inventory: {c.BLUE}I{c.RESET}\n"
                  f"{c.RED}Quit: {c.BLUE}Q{c.RESET}\n"
                  f"What would you like to do? ")
        user_input = input(prompt)

        #   Only allow valid input.
        while user_input.upper() not in room.dirs and user_input.upper() != 'C' and user_input.upper() != 'I' and user_input.upper() != 'Q':
            print(f"{c.RED}Invalid input{c.RESET}")
            user_input = input(prompt)

        #   Check stats and active item information.
        if user_input.upper() == 'C':
            self.player.stats()

        #   Check active item inventory.
        if user_input.upper() == 'I':
            print("Inventory:")
            self.player.active_items()

        #   Check which direction the player wants to go and return that room.
        if user_input.upper() in room.dirs:
            for i in range(len(room.connected_rooms)):
                if user_input.upper() == room.connected_rooms[i][0]:
                    return room.connected_rooms[i][1]

        #   Quit the game.
        if user_input.upper() == 'Q':
            make_sure = input("Are you sure you want to quit (Y/N)? ")
            if make_sure.upper() == 'Y':
                self.game_over = True
                print(f"\n{c.RED}You've quit {c.VIOLET}The Empress's Perseverance{c.RED}. We hope you've enjoyed your adventure!")
        return room

    #   Loop for the final boss fight.
    def empress_battle(self):
        print(f"{c.VIOLET}It's time for the final battle against {c.EMPRESS}.{c.RESET}\n")

        #   Round accumulator
        i = 0

        #   Loop until either the boss is dead, the player is dead, or the player gives up.
        while self.empress.health > 0 and self.player.health > 0 and not self.game_over:
            print(f"{c.YELLOW}========================= {c.VIOLET}Battle with {c.EMPRESS}{c.YELLOW}"
                  f" - Round: {i + 1}{c.YELLOW} =========================={c.RESET}\n")
            print(f"Player's health: {self.player.health}\n"
                  f"{c.VIOLET}{c.EMPRESS}'s{c.RESET} health: {self.empress.health}\n")

            #   Cycle through the Empress's attack phases
            empress_attack_num = i % 4
            if empress_attack_num == 0:
                empress_attack = c.FIRST_ATTACK
                empress_attack_damage = self.empress.first_attack()
            elif empress_attack_num == 1:
                empress_attack = c.SECOND_ATTACK
                empress_attack_damage = self.empress.second_attack()
            elif empress_attack_num == 2:
                empress_attack = c.START_THIRD_ATTACK
                empress_attack_damage = self.empress.start_third_attack()
            elif empress_attack_num == 3:
                empress_attack = c.FINISH_THIRD_ATTACK
                empress_attack_damage = self.empress.finish_third_attack()

            #   Recharge the light arrows, every tenth round when the charges have dropped to 0.
            if self.player.light_arrow.charges == 0 and i % 10 == 0:
                self.player.light_arrow.charges += 1

            #   Determine the Player's path
            user_input = input(f"The Empress is using {c.GREEN}{empress_attack}{c.RESET}.\n\n"
                               f"Options:\n"
                               f"A. Attack with {c.RED}{c.SWORD}{c.RESET}\n"
                               f"B. Attack with {c.YELLOW}{c.LIGHT_ARROW}{c.RESET} ({self.player.light_arrow.charges}/5 charges remaining)\n"
                               f"C. Defend with {c.INDIGO}{c.SHIELD}{c.RESET}\n"
                               f"Any other entry: {c.ORANGE}Give up{c.RESET}\n\n"
                               f"What will you do? ")

            #   Increment the round accumulator
            i += 1

            #   Use the sword
            if user_input.upper() == 'A':
                damage = self.player.sword.use_weapon()

                #   Cloak bonus every sixth round.
                if i % 6 == 0:
                    damage *= 2
                    print(f"{c.YELLOW}{c.CLOAK}{c.RESET} aided you in a stealthy attack and doubled "
                          f"{c.YELLOW}{c.SWORD}'s{c.RESET} damage.")

                #   Take the damage dealt by the boss as no defense was used.
                self.player.health -= empress_attack_damage

                #   Only allow the boss to be killed by the light arrows.
                if self.empress.health - damage > 0:
                    self.empress.health -= damage
                else:
                    print(f"{c.VIOLET}{c.EMPRESS}{c.RESET} cannot be killed by {c.YELLOW}{c.SWORD}{c.RESET}.\n"
                          f"{c.RED}You need to try another option.{c.RESET}")
                print(f"{c.YELLOW}{c.SWORD}{c.RESET} dealt {c.RED}{damage}{c.RESET} damage\n")

            #   Use the light arrows
            elif user_input.upper() == 'B':
                damage = self.player.light_arrow.use_weapon()

                #   Cloak bonus every fifth round.
                if i % 5 == 0:
                    self.player.health += 25
                    print(f"{c.YELLOW}{c.CLOAK}{c.RESET} allowed you to take your time lining up the shot for "
                          f"{c.YELLOW}{c.LIGHT_ARROW}{c.RESET} and healed you a little.")

                #   Take the damage dealt by the boss as no defense was used.
                self.player.health -= empress_attack_damage

                #   Deal damage to the boss.
                self.empress.health -= damage
                print(f"{c.YELLOW}{c.LIGHT_ARROW}{c.RESET} dealt {c.RED}{damage}{c.RESET} damage\n")

            #   Use the shield
            elif user_input.upper() == 'C':
                prevented_damage = self.player.shield.use_defense()

                #   Cloak bonus every twelfth round.
                if i % 12 == 0:
                    print(f"{c.YELLOW}{c.CLOAK}{c.RESET} coupled with {c.YELLOW}{c.LIGHT_ARROW}{c.RESET} "
                          f"prevented all damage.")

                #   When the prevented damage denominator is >= 50, prevent all damage.
                #   Otherwise, take the reduced damage.
                elif prevented_damage >= 50:
                    print(f"All damage was prevented by {c.YELLOW}{c.SHIELD}{c.RESET}: "
                          f"{c.RED}{empress_attack_damage}{c.RESET} would have been inflicted.")
                else:
                    self.player.health -= int(empress_attack_damage / prevented_damage)

            #   Indecision path
            elif user_input.upper() == '':
                self.player.health -= empress_attack_damage
                print(f"{c.RED}Your indecision has cost you and {c.VIOLET}{c.EMPRESS}{c.RED} dealt {c.BLUE}{empress_attack_damage}{c.RESET}.")

            #   Quit the game
            else:
                self.game_over = True
                print(f"{c.RED}The pressure of {c.VIOLET}The Empress's{c.RED} cruelty was too much for you and you gave up...")
                return

            #   Double KO
            if self.empress.health <= 0 and self.player.health <= 0:
                print(f"{c.BLUE}Both you and {c.VIOLET}{c.EMPRESS}{c.BLUE} have been felled in the battle.\n"
                      f"{c.YELLOW}You've died, but you've also protected the world from the evil contained herein!!!")
                self.game_over = True
                return

            #   Killed the empress
            if self.empress.health <= 0:
                print(f"{c.BLUE}You've defeated {c.VIOLET}The Empress{c.BLUE} and ended the cruel reign of the Empire.\n"
                      f"{c.YELLOW}You win!!!")
                self.game_over = True
                return

            #   Player is dead
            if self.player.health <= 0:
                print(f"{c.RED}You've been slain by {c.VIOLET}The Empress{c.RED}. Better luck next time, warrior.\n"
                      f"Game over...{c.RESET}")
                self.game_over = True
                return
