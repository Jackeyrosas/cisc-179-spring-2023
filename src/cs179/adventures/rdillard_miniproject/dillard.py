import constants as c
import game

#   Instantiate the game object.
g = game.Game()

#   Print the details of the starting room and set the room the player is in.
g.rooms[0].print_room()
in_room = g.rooms[0]

#   Run the game until the player wins, dies, or quits.
while not g.game_over:
    prev_room = in_room
    in_room = g.take_user_input(in_room)
    if in_room.description[0] == c.EMPRESS_KEEP and g.player.active_items() == 10:
        print(f"{c.YELLOW}You've discovered all of the secrets in the castle and used the {c.RED}{c.BOMB}{c.YELLOW} "
              f"to enter {c.VIOLET}{c.EMPRESS_KEEP}{c.RESET}\n")
        g.enter_room(in_room)
        g.game_over = True
    elif in_room.description[0] == c.EMPRESS_KEEP and g.player.active_items() != 10:
        print(f"{c.YELLOW}You can't enter here without discovering all of the secrets in the castle.\n"
              f"{c.RED}You've been turned back.{c.RESET}")
        in_room = prev_room
        g.enter_room(in_room)
    elif prev_room != in_room:
        g.enter_room(in_room)
