import turtle
import constants as c


#   If you wanted to have a displayed map for the room you're in, run these functions and take screenshots.
class MapGenerator:
    def __init__(self, a_turtle, window, player):
        self.a_turtle = a_turtle
        self.window = window
        self.player = player

    def restore_settings(self, room_name):
        self.window.clear()
        self.window.title(room_name)
        self.window.bgcolor('black')

        self.a_turtle = turtle.Turtle()
        self.a_turtle.color('white')
        self.a_turtle.width(5)
        self.a_turtle.speed(0)
        self.a_turtle.hideturtle()

    def draw_the_barracks(self, x=None, y=None):
        self.restore_settings(c.BARRACKS)

        self.a_turtle.color('white')
        self.a_turtle.width(5)
        self.a_turtle.speed(0)
        self.a_turtle.hideturtle()

        self.a_turtle.penup()
        self.a_turtle.backward(300)
        self.a_turtle.right(90)
        self.a_turtle.forward(300)
        self.a_turtle.pendown()
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Proving Ground", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(600)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Starting Room", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("West Corridor North", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(300)

    def draw_the_chapel(self, x=None, y=None):
        self.restore_settings(c.CHAPEL)

        self.a_turtle.color('purple')
        self.a_turtle.write("The Priest has restored your health", font=("Old English Text MT", 15, "normal"),
                            align='center')
        self.a_turtle.color('white')
        self.a_turtle.penup()
        self.a_turtle.goto(-350, -350)
        self.a_turtle.pendown()
        self.a_turtle.forward(350)
        self.a_turtle.write("Graveyard", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(350)
        self.a_turtle.left(90)
        self.a_turtle.forward(700)
        self.a_turtle.left(90)
        self.a_turtle.forward(350)
        self.a_turtle.write("Northeast Corridor", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(350)
        self.a_turtle.left(90)
        self.a_turtle.forward(700)

    def draw_the_east_cell(self, x=None, y=None):
        self.restore_settings(c.EAST_CELL)

        self.a_turtle.penup()
        self.a_turtle.goto(0, -150)
        self.a_turtle.pendown()
        self.a_turtle.write("East Corridor", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(150)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(150)
        if not self.player.shield.active:
            self.a_turtle.penup()
            self.a_turtle.goto(0, 75)
            self.a_turtle.color('blue')
            self.a_turtle.write("Shield", font=("Old English Text MT", 15, "normal"), align='center')

    def draw_the_east_corridor(self, x=None, y=None):
        self.restore_settings(c.EAST_CORRIDOR)

        self.a_turtle.penup()
        self.a_turtle.right(90)
        self.a_turtle.forward(300)
        self.a_turtle.right(90)
        self.a_turtle.forward(200)
        self.a_turtle.pendown()
        self.a_turtle.right(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("West", font=("Old English Text MT", 15, "normal"), align='right')
        self.a_turtle.forward(50)
        self.a_turtle.right(90)
        self.a_turtle.forward(200)
        self.a_turtle.left(90)
        self.a_turtle.forward(500)
        self.a_turtle.right(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("North", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.right(90)
        self.a_turtle.forward(600)
        self.a_turtle.right(90)
        self.a_turtle.forward(300)

    def draw_the_empress_keep(self, x=None, y=None):
        self.restore_settings(c.EMPRESS_KEEP)

        self.a_turtle.penup()
        self.a_turtle.goto(-400, 300)
        self.a_turtle.pendown()
        self.a_turtle.width(20)
        self.a_turtle.color('red')
        self.a_turtle.forward(400)
        self.a_turtle.write("No Escape from The Empress's Keep", font=("Old English Text MT", 30, "normal"),
                            align='center')
        self.a_turtle.forward(400)
        self.a_turtle.right(90)
        self.a_turtle.forward(600)
        self.a_turtle.right(90)
        self.a_turtle.forward(800)
        self.a_turtle.right(90)
        self.a_turtle.forward(600)
        self.a_turtle.penup()
        self.a_turtle.goto(0, 0)
        self.a_turtle.write("The Empress", font=("Old English Text MT", 30, "normal"), align='center')

    def draw_the_graveyard(self, x=None, y=None):
        self.restore_settings(c.GRAVEYARD)

        self.a_turtle.penup()
        self.a_turtle.goto(-400, 300)
        self.a_turtle.pendown()
        self.a_turtle.forward(700)
        self.a_turtle.write("Chapel", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(600)
        self.a_turtle.right(90)
        self.a_turtle.forward(800)
        self.a_turtle.right(90)
        self.a_turtle.forward(150)
        self.a_turtle.write("Proving Ground", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(450)
        self.a_turtle.penup()
        self.a_turtle.goto(-350, 275)
        self.a_turtle.pendown()
        self.a_turtle.color('purple')
        self.a_turtle.right(90)
        self.a_turtle.forward(75)
        self.a_turtle.write("Mausoleum", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(75)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(150)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.penup()
        if not self.player.power_up_one.active:
            self.a_turtle.goto(-275, 250)
            self.a_turtle.color('red')
            self.a_turtle.write("Emperor's Might", font=("Old English Text MT", 15, "normal"), align='center')
            self.a_turtle.goto(-275, 225)
            self.a_turtle.color('gold')
            self.a_turtle.write("Key Two", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.color('white')
        self.a_turtle.goto(225, -125)
        self.a_turtle.pendown()
        self.a_turtle.color('brown')
        self.a_turtle.right(90)
        self.a_turtle.forward(150)
        self.a_turtle.right(105)
        self.a_turtle.forward(150)
        self.a_turtle.right(75)
        self.a_turtle.forward(150)
        self.a_turtle.right(105)
        self.a_turtle.forward(150)
        self.a_turtle.right(75)
        self.a_turtle.forward(25)
        self.a_turtle.color('gray')
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.penup()
        if not self.player.light_arrow.active:
            self.a_turtle.goto(280, -200)
            self.a_turtle.color('yellow')
            self.a_turtle.write("Light Arrow", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.goto(300, -75)
        self.a_turtle.color('gray')
        self.a_turtle.write("RIP", font=("Old English Text MT", 15, "normal"), align='center')

    def draw_the_northeast_corridor(self, x=None, y=None):
        self.restore_settings(c.NORTHEAST_CORRIDOR)

        self.a_turtle.penup()
        self.a_turtle.goto(300, 300)
        self.a_turtle.pendown()
        self.a_turtle.left(180)
        self.a_turtle.forward(600)
        self.a_turtle.left(90)
        self.a_turtle.forward(75)
        self.a_turtle.write("Throne Room", font=("Old English Text MT", 12, "normal"))
        self.a_turtle.forward(75)
        self.a_turtle.left(90)
        self.a_turtle.forward(450)
        self.a_turtle.right(90)
        self.a_turtle.forward(450)
        self.a_turtle.left(90)
        self.a_turtle.forward(75)
        self.a_turtle.write("Chapel", font=("Old English Text MT", 12, "normal"), align='center')
        self.a_turtle.forward(75)
        self.a_turtle.left(90)
        self.a_turtle.forward(600)
        self.a_turtle.penup()
        self.a_turtle.goto(150, 0)
        self.a_turtle.color('red')
        self.a_turtle.right(90)
        self.a_turtle.pendown()
        self.a_turtle.forward(150)
        if not self.player.bomb.active:
            self.a_turtle.penup()
            self.a_turtle.goto(225, -50)
            self.a_turtle.color('brown')
            self.a_turtle.write("Bomb", font=("Old English Text MT", 12, "normal"), align='center')

    def draw_the_proving_ground(self, x=None, y=None):
        self.restore_settings(c.PROVING_GROUND)

        self.a_turtle.penup()
        self.a_turtle.backward(300)
        self.a_turtle.right(90)
        self.a_turtle.forward(300)
        self.a_turtle.pendown()
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Staircase", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Graveyard", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Barracks", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(600)
        self.a_turtle.penup()
        self.a_turtle.color('yellow')
        self.a_turtle.goto(-250, -150)
        self.a_turtle.pendown()
        self.a_turtle.write("Key", font=("Old English Text MT", 15, "normal"), align='center')

    def draw_the_spire(self, x=None, y=None):
        self.restore_settings(c.SPIRE)

        self.a_turtle.penup()
        self.a_turtle.goto(300, 0)
        self.a_turtle.left(90)
        self.a_turtle.pendown()

        for i in range(20):
            self.a_turtle.forward(100)
            self.a_turtle.left(18)

        self.a_turtle.forward(50)

        angle_to_turn = 20
        distance_to_move = 120
        for i in range(25):
            self.a_turtle.left(angle_to_turn + i)
            self.a_turtle.forward(distance_to_move - i * 2)

        self.a_turtle.penup()
        self.a_turtle.goto(0, 0)
        self.a_turtle.color('brown')
        self.a_turtle.write("Cloak", font=("Old English Text MT", 12, "normal"), align='center')
        self.a_turtle.goto(305, 0)
        self.a_turtle.color('white')
        self.a_turtle.write("Throne Room", font=("Old English Text MT", 15, "normal"))

    def draw_the_staircase(self, x=None, y=None):
        self.restore_settings(c.STAIRCASE)

        self.a_turtle.penup()
        self.a_turtle.forward(400)
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.pendown()
        self.a_turtle.left(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("Proving Ground", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(600)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.left(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("West Corridor North", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.left(90)
        self.a_turtle.forward(200)
        self.a_turtle.left(90)
        self.a_turtle.forward(800)
        self.a_turtle.left(90)
        self.a_turtle.forward(200)

    def draw_the_starting_room(self, x=None, y=None):
        self.restore_settings(c.STARTING_ROOM)

        self.a_turtle.penup()
        self.a_turtle.backward(300)
        self.a_turtle.right(90)
        self.a_turtle.forward(300)
        self.a_turtle.pendown()
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Barracks", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("East Corridor", font=("Old English Text MT", 15, "normal"), align='right')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("Throne Room", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.write("West Corridor North", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(300)

    def draw_the_throne_room(self, x=None, y=None):
        self.restore_settings(c.THRONE_ROOM)

        self.a_turtle.penup()
        self.a_turtle.goto(-400, 300)
        self.a_turtle.pendown()
        self.a_turtle.forward(400)
        self.a_turtle.write("Empress's Keep", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(400)
        self.a_turtle.right(90)
        self.a_turtle.forward(150)
        self.a_turtle.write("Northeast Corridor", font=("Old English Text MT", 15, "normal"), align='right')
        self.a_turtle.forward(450)
        self.a_turtle.right(90)
        self.a_turtle.forward(400)
        self.a_turtle.write("Starting Room", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(400)
        self.a_turtle.right(90)
        self.a_turtle.forward(450)
        self.a_turtle.write("The Spire", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(150)
        self.a_turtle.goto(-400, -150)
        self.a_turtle.right(90)
        self.a_turtle.color('red')
        self.a_turtle.forward(75)
        self.a_turtle.write("The West Cell", font=("Old English Text MT", 12, "normal"), align='center')
        self.a_turtle.forward(75)
        self.a_turtle.right(90)
        self.a_turtle.forward(150)
        self.a_turtle.penup()
        if not self.player.sword.active:
            self.a_turtle.goto(-325, -200)
            self.a_turtle.color('blue')
            self.a_turtle.write("Sword", font=("Old English Text MT", 12, "normal"), align='center')
            self.a_turtle.color('red')
        self.a_turtle.goto(250, -300)
        self.a_turtle.pendown()
        self.a_turtle.left(180)
        self.a_turtle.forward(150)
        self.a_turtle.right(90)
        self.a_turtle.forward(75)
        self.a_turtle.write("The East Cell", font=("Old English Text MT", 12, "normal"), align='center')
        self.a_turtle.forward(75)
        self.a_turtle.penup()
        if not self.player.shield.active:
            self.a_turtle.goto(325, -200)
            self.a_turtle.color('blue')
            self.a_turtle.write("Shield", font=("Old English Text MT", 12, "normal"), align='center')

    def draw_the_west_cell(self, x=None, y=None):
        self.restore_settings(c.WEST_CELL)

        self.a_turtle.penup()
        self.a_turtle.goto(0, -150)
        self.a_turtle.pendown()
        self.a_turtle.write("West Corridor North", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(150)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(150)
        if not self.player.sword.active:
            self.a_turtle.penup()
            self.a_turtle.goto(0, 75)
            self.a_turtle.color('blue')
            self.a_turtle.write("Sword", font=("Old English Text MT", 15, "normal"), align='center')

    def draw_the_west_corridor_north(self, x=None, y=None):
        self.restore_settings(c.WEST_CORRIDOR_NORTH)

        self.a_turtle.penup()
        self.a_turtle.forward(200)
        self.a_turtle.right(90)
        self.a_turtle.forward(350)
        self.a_turtle.pendown()
        self.a_turtle.left(180)
        self.a_turtle.forward(100)
        self.a_turtle.write("Starting Room", font=("Old English Text MT", 15, "normal"))
        self.a_turtle.forward(100)
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(500)
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.write("West Cell", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(100)
        self.a_turtle.left(90)
        self.a_turtle.forward(700)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)

    def draw_the_west_corridor_south(self, x=None, y=None):
        self.restore_settings(c.WEST_CORRIDOR_SOUTH)

        self.a_turtle.penup()
        self.a_turtle.goto(100, -50)
        self.a_turtle.left(90)
        self.a_turtle.pendown()
        self.a_turtle.forward(50)
        self.a_turtle.write("Barracks", font=("Old English Text MT", 15, "normal"), align='left')
        self.a_turtle.forward(50)
        self.a_turtle.left(90)
        self.a_turtle.forward(100)
        self.a_turtle.right(90)
        self.a_turtle.forward(300)
        self.a_turtle.left(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("West Corridor North", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.left(90)
        self.a_turtle.forward(700)
        self.a_turtle.left(90)
        self.a_turtle.forward(50)
        self.a_turtle.write("Staircase", font=("Old English Text MT", 15, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.left(90)
        self.a_turtle.forward(300)
        self.a_turtle.right(90)
        self.a_turtle.forward(100)
        self.a_turtle.penup()
        self.a_turtle.goto(-100, -150)
        self.a_turtle.pendown()
        self.a_turtle.color('red')
        self.a_turtle.forward(50)
        self.a_turtle.write("2nd Level", font=("Old English Text MT", 12, "normal"), align='center')
        self.a_turtle.forward(50)
        self.a_turtle.penup()
        self.a_turtle.goto(-50, -200)
        self.a_turtle.pendown()
        self.a_turtle.color('Brown')
        self.a_turtle.write("Shovel", font=("Old English Text MT", 12, "normal"), align='center')

    def draw_room(self, room_name):
        fxn = None
        if room_name == c.BARRACKS:
            fxn = self.draw_the_barracks
        if room_name == c.CHAPEL:
            fxn = self.draw_the_chapel
        if room_name == c.EAST_CELL:
            fxn = self.draw_the_east_cell
        if room_name == c.EAST_CORRIDOR:
            fxn = self.draw_the_east_corridor
        if room_name == c.EMPRESS_KEEP:
            fxn = self.draw_the_empress_keep
        if room_name == c.GRAVEYARD:
            fxn = self.draw_the_graveyard
        if room_name == c.NORTHEAST_CORRIDOR:
            fxn = self.draw_the_northeast_corridor
        if room_name == c.PROVING_GROUND:
            fxn = self.draw_the_proving_ground
        if room_name == c.SPIRE:
            fxn = self.draw_the_spire
        if room_name == c.STAIRCASE:
            fxn = self.draw_the_staircase
        if room_name == c.STARTING_ROOM:
            fxn = self.draw_the_starting_room
        if room_name == c.THRONE_ROOM:
            fxn = self.draw_the_throne_room
        if room_name == c.WEST_CELL:
            fxn = self.draw_the_west_cell
        if room_name == c.WEST_CORRIDOR_NORTH:
            fxn = self.draw_the_west_corridor_north
        if room_name == c.WEST_CORRIDOR_SOUTH:
            fxn = self.draw_the_west_corridor_south
        return fxn
