maze = ["edge","edge","edge","edge","edge","edge","edge","edge","edge","edge"]
maze = maze + ["edge","","1000","","","","","","","edge"]
maze = maze + ["edge","","500","dark","pit","","life","100","","edge"]
maze = maze + ["edge","","100","100","dark","pit","dark","life","","edge"]
maze = maze + ["edge","","life","dark","2024","life","pit","100","","edge"]
maze = maze + ["edge","life","100","pit","light","100","","500","","edge"]
maze = maze + ["edge","light","500","dark","100","","","1000","","edge"]
maze = maze + ["edge","","1000","life","500","","","dark","","edge"]
maze = maze + ["edge","dark","pit","dark","1000","life","dark","pit","dark","edge"]
maze = maze + ["edge","edge","edge","edge","edge","edge","edge","edge","edge","edge"]
print ("Welcome to the time maze")
print ("You were in 2024 when you entered the maze, but now you are in the year 24")
print ("The only way out is to navigate the time maze finding portals that will change time")
print ("You may do North, South, East, West N S E W and find new rooms")
print ("You start with 1 life and some rooms have extra lifes")
print ("You may find rooms that will light your torch which starts out lit")
print ("You may find rooms that extinguish your torch")
print ("If your torch is out you might fall into a pit losing a life")
print ("You may run into the edge of the maze and have to take a different direction")
print ("Once your time is returned to 2024 you can return to the starting point to exit the maze ")
print ("Each time you hit a time portal it will change that time portal to the opposite of what it was")
print ("If your time is not 2024 you must seek out rooms to restore time to 2024")
print ("There are many solutions to win and falling to the pit when you have no lives left will also end the game")
print ("Have a good time")
print ("")
time_code = 24
lifes = 1
torch = True
location = 44
while time_code != 2024 or location != 44:
    print("The year is ", time_code)
#    print ("location ",location)
#    print ("current",maze[location])
#    print ("N",maze[location-10])
#    print ("S",maze[location+10])
#    print ("W",maze[location-1])
#    print ("E",maze[location+1])
    print ("")
    str = input("N,S,E,W?")
    str = str.upper()
    next_location = location
    save_location = maze[location]
    if str == "N":
        next_location = location - 10
    if str == "S":
        next_location = location + 10
    if str == "E":
        next_location = location + 1
    if str == "W":
        next_location = location - 1
    if maze[next_location] == "edge":
        print ("You have reached the edge and can not go that way")
    if maze[next_location] == "pit" and torch == True:
        print ("You see a pit and do not enter the room")
    if maze[next_location] == "pit" and torch == False:
        print ("You fell into a pit and lose a life")
        lifes = lifes - 1
        maze[next_location] = ""
        location = next_location
        if lifes == 0:
            location = 44
            next_location = 44
            time_code = 2024
    if maze[next_location] == "life":
        print ("One additional life")
        lifes = lifes + 1
        location = next_location
        save_location = "life"
    if maze[next_location] == "dark":
        print ("Your torch is out")
        torch = False
        location = next_location
        save_location = "dark"
    if maze[next_location] == "light":
        print ("Your torch is lit")
        torch = True
        location = next_location
        save_location = "light"
    if maze[next_location] == "100":
        time_code = time_code + 100
        print ("Time portal +100")
        location = next_location
        save_location = "-100"
    if maze[next_location] == "-100":
        time_code = time_code - 100
        print ("Time portal -100")
        location = next_location
        save_location = "100"
    if maze[next_location] == "500":
        time_code = time_code + 500
        print ("Time portal + 500")
        location = next_location
        save_location = "-500"
    if maze[next_location] == "-500":
        time_code = time_code - 500
        print ("Time portal -500")
        location = next_location
        save_location = "500"
    if maze[next_location] == "1000":
        time_code = time_code + 1000
        print ("Time portal +1000")
        location = next_location
        save_location = "-1000"
    if maze[next_location] == "-1000":
        time_code = time_code - 1000
        print ("Time portal -1000")
        location = next_location
        save_location = "1000"
    if maze[next_location] == "":
        location = next_location
        save_location = ""
    if maze[next_location] == "2024":
        location = next_location
        save_location = "2024"
    maze[location]= save_location
if lifes > 0:
    print("You have solved the maze")
else: 
    print ("You have failed the maze")
        
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

